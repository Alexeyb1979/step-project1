/*Start tabs Servises*/

        $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });
/*Finish tabs*/




/*Start Slider*/
const slider = $('#slider');
	let sliderWrap = $('#slider ul');
	let sliderImg = $('#slider ul li');
	let length = sliderImg.length;
	let width = sliderImg.width();
	let thumbWidth = width/length;

	sliderWrap.width(width*(length+2));

	//Set up
	slider.after('<div id="' + 'pager' + '"></div>');
	var dataVal = 1;
$('#pager').append('<a href="#" class="slider-arrow" id="sliderNext">&gt;</a>');
	sliderImg.each(
		function(){
			$(this).attr('data-img',dataVal);
			$('#pager').append('<a class="slider-thumbnail" data-img="' + dataVal + '"><img src=' + $('img', this).attr('src') + ' width=' + thumbWidth + '></a>');
		dataVal++;
	});
$('#pager').append('<a href="#" class="slider-arrow" id="sliderPrev">&lt;</a>');
	
	let prevBtm = $('#sliderPrev');
	let nextBtm = $('#sliderNext');

	//Copy 2 images and put them in the front and at the end
	$('#slider ul li:first-child').clone().appendTo('#slider ul');
	$('#slider ul li:nth-child(' + length + ')').clone().prependTo('#slider ul');

	sliderWrap.css('margin-left', - width);
	$('#slider ul li:nth-child(2)').addClass('active');

	let imgPos = pagerPos = $('#slider ul li.active').attr('data-img');
	$('#pager a:nth-child(' +pagerPos+ ')').addClass('active');


	//Click on Pager  
	$('#pager a').on('click', function() {
		pagerPos = $(this).attr('data-img');
		$('#pager a.active').removeClass('active');
		$(this).addClass('active');

		if (pagerPos > imgPos) {
			var movePx = width * (pagerPos - imgPos);
			moveNext(movePx);
		}

		if (pagerPos < imgPos) {
			var movePx = width * (imgPos - pagerPos);
			movePrev(movePx);
		}
		return false;
	});

	//Click on Buttons
	nextBtm.on('click', function(){
        moveNext(width);
		return false;
	});

	prevBtm.on('click', function(){
        movePrev(width);
		return false;
	});

	//Function for pager active motion
	function pagerActive() {
		pagerPos = imgPos;
		$('#pager a.active').removeClass('active');
		$('#pager a[data-img="' + pagerPos + '"]').addClass('active');
	}

	//Function for moveNext Button
	function moveNext(moveWidth) {
		sliderWrap.animate({
    		'margin-left': '-=' + moveWidth
  			}, 50, function() {
  				if (imgPos==length) {
  					imgPos=1;
  					sliderWrap.css('margin-left', - width);
  				}
  				else if (pagerPos > imgPos) {
  					imgPos = pagerPos;
  				} 
  				else {
  					imgPos++;
  				}
  				pagerActive();
  		});
	}

	//Function for movePrev Button
	function movePrev(moveWidth) {
		sliderWrap.animate({
    		'margin-left': '+=' + moveWidth
  			}, 500, function() {
  				if (imgPos==1) {
  					imgPos=length;
  					sliderWrap.css('margin-left', -(width*length));
  				}
  				else if (pagerPos < imgPos) {
  					imgPos = pagerPos;
  				} 
  				else {
  					imgPos--;
  				}
  				pagerActive();
  		});
	}

/*Finish slider*/

/*Start Gallery*/
$(".tab-menu-section3").click(function () {
    $(this).addClass("active").siblings().removeClass("active");
    $('.amazing-work-item').hide();
    const category = $(this).attr("data-target");
    if(category === "all") {
        $('.amazing-work-item').slice(0, 12).show();
    }
    else {
        $('.amazing-work-item[data-type="'+category+'"]').show();
    }
});

$(".amazing-work-item").slice(12).hide();

$("#loadMore").click(function () {
    $(".amazing-work-item:hidden").slice(0, 12).show(500);
    if ($(".amazing-work-item:hidden").length === 0) {
        $(this).hide(500);
    }
});
$(".amazing-work-item").hover(function () {
    let moreInfo = $(this).attr("data-info");
    moreInfo = moreInfo.split('; ');
    $(this).prepend(`<div class="amazing-work-item-info">
                        <div class="amazing-work-info-icons">
                            <a href="#" class="amazing-work-icon"><i class="fas fa-link"></i></a>
                            <a href="#" class="amazing-work-icon"><i class="fas fa-search"></i></a>

                        </div>
                        <h3 class="amazing-work-info-title">${moreInfo[0]}</h3>
                        <p class="amazing-work-info-description">${moreInfo[1]}</p>
                    </div>`);
}, function () {
    $(this).find(".amazing-work-item-info").remove();
});

/*Finish Gallary*/




